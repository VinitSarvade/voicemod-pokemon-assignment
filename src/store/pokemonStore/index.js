import { pokemons } from '../../../data/pokemons.json';

export default {
    namespaced: true,
    state: {
      pokemons,
      searchString: '',
      filterType: null,
    },
    mutations: {
      setSearch(state, data) {
        state.searchString = data;
      },
      setFilter(state, filter) {
        state.filterType = filter;
      }
    },
    actions: {
      setSearch: (context, data) => {
        context.commit('setSearch', data);
      },
      setFilter: (context, filter) => {
        context.commit('setFilter', filter);
      }
    }
}